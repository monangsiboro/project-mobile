package com.example.aplikasicoba;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Quiz extends AppCompatActivity {

    EditText ed1;
    TextView tv1,tv2,tv3;
    RadioButton a,b,c,d;
    Button bt,bt2;
    RadioGroup rg;
    int q,s;
    ImageView image_senyum,image_sedih,image_marah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        ed1=(EditText) findViewById(R.id.name);
        tv1=(TextView)findViewById(R.id.ques);
        tv2=(TextView)findViewById(R.id.response);
        tv3=(TextView)findViewById(R.id.score);
        rg=(RadioGroup)findViewById(R.id.optionGroup);
        a=(RadioButton)findViewById(R.id.option1);
        b=(RadioButton)findViewById(R.id.option2);
        c=(RadioButton)findViewById(R.id.option3);
        d=(RadioButton)findViewById(R.id.option4);
        bt=(Button)findViewById(R.id.next);
        bt2=(Button)findViewById(R.id.Exit);
        image_senyum = (ImageView) findViewById(R.id.emotmuncul_senyum);
        image_sedih = (ImageView) findViewById(R.id.emotmuncul_sedih);
        image_marah = (ImageView) findViewById(R.id.emotmuncul_marah);
        q=0;
        s=0;

    }
    public void quiz(View v){
        switch (q){
            case 0:
            {
                ed1.setVisibility(View.INVISIBLE);
                rg.setVisibility(View.VISIBLE);
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                tv2.setText("");
                tv3.setText("");
                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                ed1.setEnabled(true);
                bt.setText("Next");
                s=0;

                tv1.setText("1. 100x5 = ?");
                a.setText("500");
                b.setText("10");
                c.setText("11");
                d.setText("12");
                q=1;
                break;
            }
            case 1:
            {
                ed1.setVisibility(View.INVISIBLE);
                ed1.setEnabled(false);
                tv1.setText("2. (3-5)x10 = ?");
                a.setText("-20");
                b.setText("2000");
                c.setText("101");
                d.setText("100");

                if (a.isChecked())
                {
                    //Memanggil Fungsi JawabBenar(); ada dibawah
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    //Memanggil Fungsi TidakDiJawab(); ada dibawah
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    //Memanggil Fungsi SalahJawab(); ada dibawah
                    SalahJawab();
                }
                q=2;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 2:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("3. 12+7+53-10 ?");
                a.setText("10");
                b.setText("20");
                c.setText("30");
                d.setText("62");
                if (d.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=3;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 3:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("4. 65x3= ?");
                a.setText("195");
                b.setText("100");
                c.setText("500");
                d.setText("Dobel 12");
                if (d.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=4;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 4:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("5. 9-2-5 = ?");
                a.setText("2");
                b.setText("200");
                c.setText("-15");
                d.setText("500");
                if (a.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=5;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 5:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("6. 500x500 = ?");
                a.setText("1");
                b.setText("250000");
                c.setText("2");
                d.setText("3");
                if (c.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=6;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 6:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("7. akar dari 625 = ?");
                a.setText("1");
                b.setText("25");
                c.setText("2");
                d.setText("3");
                if (b.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=7;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 7:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("8. 7 pangkat 2 = ? ");
                a.setText("1");
                b.setText("49");
                c.setText("2600");
                d.setText("3");
                if (b.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=8;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 8:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("9. 1+1 = ? ");
                a.setText("30000");
                b.setText("1000");
                c.setText("2");
                d.setText("3");
                if (c.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=9;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 9:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("10.(3x5)-(5+5)+19 = ?");
                a.setText("24");
                b.setText("1000");
                c.setText("2000");
                d.setText("345");
                if (a.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=10;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                bt2.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v)
                                           {
                                               Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                               finish();
                                               System.exit(0);
                                           }
                                       }
                );
                break;
            }
            case 10:
            {
                ed1.setVisibility(View.INVISIBLE);
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                if (a.isChecked())
                {
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                tv3.setText("Hai "+ed1.getText()+"\nfinal score is "+s);
                bt.setText("Restart");
                q=0;
                bt2.setOnClickListener(new View.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(View v)
                                            {
                                                Toast.makeText(getApplicationContext(), "Keluar Quiz", Toast.LENGTH_SHORT).show();
                                                finish();
                                                System.exit(0);
                                            }
                                        }
                );
                break;
            }
        }
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void JawabBenar()
    {
        tv2.setText("Jawaban Benar");
        s=s+50;
        image_senyum.setVisibility(View.VISIBLE);
        image_senyum.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_senyum.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void TidakDiJawab()
    {
        tv2.setText("Mohon Maaf Jawaban Tidak Dipilih");
        s=s+0;
        image_marah.setVisibility(View.VISIBLE);
        image_marah.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_marah.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void SalahJawab()
    {
        tv2.setText("Mohon Maaf Jawaban Salah");
        s=s-10;
        image_sedih.setVisibility(View.VISIBLE);
        image_sedih.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_sedih.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }


}